'use strict';

window.addEventListener('DOMContentLoaded', function () {
    /*verifier le chargement du DOMContentLoading*/
    console.log("DOM entièrement chargé et analysé");

    /**Menu burger** */
    const burgerMenu = document.querySelector('.burgerMenu');
    burgerMenu.addEventListener('click', event => {
        const menuFront = document.querySelector('#myMenuFront');
        const header = document.querySelector('header');

        // if (menuFront.className=== menuFront){
            menuFront.classList.add('responsive');
            header.classList.add('responsiveHeader');
            burgerMenu.classList.add('hidden');
        // }
        // else{
        //     menuFront.className = menuFront;
        // }
    });


    /**1er étape cacher l'histoire de la danse au clic du menu des pas de base
     * et deplacer le menu des pas de base à la place */

    const danseMenu = document.querySelector('.dansesPasBaseMenu');
    const danseBtn = document.querySelectorAll('.buttonPasBase');
    const danseBtnH = document.querySelector('.buttonPasBaseHistoire');
    const danseBtn1 = document.querySelector('.buttonPasBase1');
    const danseBtn2 = document.querySelector('.buttonPasBase2');
    const danseBtn3 = document.querySelector('.buttonPasBase3');
    const danseBtn4 = document.querySelector('.buttonPasBase4');

    danseBtnH.addEventListener('click', event => {
        console.log('toto');
        const dansesHistoire = this.document.querySelector('.dansesHistoire');
        dansesHistoire.classList.add('translationLeft');
        dansesHistoire.classList.remove('hidden');
        danseMenu.classList.remove('translationRight');
        danseMenu.classList.add('translationLeft');

        /**2eme étape Selectionner le pas de base correspondant au bouton du menu cliqué */ 
        danseBtn.forEach(buttonsPB => {
            buttonsPB.classList.remove('select');
        });
        danseBtnH.classList.add('select');
        /**3eme étape afficher le pas de base correspondant au bouton du menu cliqué */ 
        const dansesPasBase = this.document.querySelectorAll('.dansesPasBase');
        dansesPasBase.forEach(descriptionPB => {
            descriptionPB.classList.add('hidden');
        });
    });
    
    danseBtn1.addEventListener('click', event => {
        console.log('toto');
        const dansesHistoire = this.document.querySelector('.dansesHistoire');
        dansesHistoire.classList.remove('translationLeft');
        dansesHistoire.classList.add('hidden');
        danseMenu.classList.add('translationRight');
        /**2eme étape Selectionner le pas de base correspondant au bouton du menu cliqué */ 
        danseBtn.forEach(buttonsPB => {
            buttonsPB.classList.remove('select');
        });
        danseBtn1.classList.add('select');
        /**3eme étape afficher le pas de base correspondant au bouton du menu cliqué */ 
        const dansesPasBase = this.document.querySelectorAll('.dansesPasBase');
        dansesPasBase.forEach(descriptionPB => {
            descriptionPB.classList.add('hidden');
        });
        const dansesPasBase1 = this.document.querySelector('.dansesPasBase1');
        dansesPasBase1.classList.remove('hidden');
    });

    danseBtn2.addEventListener('click', event => {
        console.log('toto');
        const dansesHistoire = this.document.querySelector('.dansesHistoire');
        dansesHistoire.classList.remove('translationLeft');
        dansesHistoire.classList.add('hidden');
        danseMenu.classList.add('translationRight');
        /**2eme étape Selectionner le pas de base correspondant au bouton du menu cliqué */ 
        danseBtn.forEach(buttonsPB => {
            buttonsPB.classList.remove('select');
        });
        danseBtn2.classList.add('select');
        /**3eme étape afficher le pas de base correspondant au bouton du menu cliqué */ 
        const dansesPasBase = this.document.querySelectorAll('.dansesPasBase');
        dansesPasBase.forEach(descriptionPB => {
            descriptionPB.classList.add('hidden');
        });
        const dansesPasBase2 = this.document.querySelector('.dansesPasBase2');
        dansesPasBase2.classList.remove('hidden');
    });

    danseBtn3.addEventListener('click', event => {
        console.log('toto');
        const dansesHistoire = this.document.querySelector('.dansesHistoire');
        dansesHistoire.classList.remove('translationLeft');
        dansesHistoire.classList.add('hidden');
        danseMenu.classList.add('translationRight');
        /**2eme étape Selectionner le pas de base correspondant au bouton du menu cliqué */ 
        danseBtn.forEach(buttonsPB => {
            buttonsPB.classList.remove('select');
        });
        danseBtn3.classList.add('select');
        /**3eme étape afficher le pas de base correspondant au bouton du menu cliqué */ 
        const dansesPasBase = this.document.querySelectorAll('.dansesPasBase');
        dansesPasBase.forEach(descriptionPB => {
            descriptionPB.classList.add('hidden');
        });
        const dansesPasBase3 = this.document.querySelector('.dansesPasBase3');
        dansesPasBase3.classList.remove('hidden');
    });

    danseBtn4.addEventListener('click', event => {
        console.log('toto');
        const dansesHistoire = this.document.querySelector('.dansesHistoire');
        dansesHistoire.classList.remove('translationLeft');
        dansesHistoire.classList.add('hidden');
        danseMenu.classList.add('translationRight');
        /**2eme étape Selectionner le pas de base correspondant au bouton du menu cliqué */ 
        danseBtn.forEach(buttonsPB => {
            buttonsPB.classList.remove('select');
        });
        danseBtn4.classList.add('select');
        /**3eme étape afficher le pas de base correspondant au bouton du menu cliqué */ 
        const dansesPasBase = this.document.querySelectorAll('.dansesPasBase');
        dansesPasBase.forEach(descriptionPB => {
            descriptionPB.classList.add('hidden');
        });
        const dansesPasBase4 = this.document.querySelector('.dansesPasBase4');
        dansesPasBase4.classList.remove('hidden');
    });
});
