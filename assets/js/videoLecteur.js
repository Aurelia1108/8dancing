'use strict';

/*****VARIABLES *******/

let video = document.querySelector('video');
let controls = document.querySelector('.controls');

let play = document.querySelector('#playpause');
let stop = document.querySelector('#stop');
let rwd = document.querySelector('#rwd');
let fwd = document.querySelector('#fwd');

let timerWrapper = document.querySelector('.timer');
let timer = document.querySelector('.timer span');
let timerBar = document.querySelector('.timer div');

let intervalFwd;
let intervalRwd;

/******Fonctions ******/
function playPauseVideo() {
  if(video.paused || video.ended) {
    video.play();
  } 
  else {
    video.pause();
  }
}

function stopVideo() {
  video.pause();
  video.currentTime = 0;
  progress.value = 0;
}

function alterVolume(dir) {
  const currentVolume = Math.floor(video.volume * 10) / 10;
  if (dir === '+' && currentVolume < 1) {
    video.volume += 0.1;
  } else if (dir === '-' && currentVolume > 0) {
    video.volume -= 0.1;
  }
}

function videoBackward() {
  clearInterval(intervalFwd);
  fwd.classList.remove('active');

  if(rwd.classList.contains('active')) {
    rwd.classList.remove('active');
    clearInterval(intervalRwd);
    video.play();
  } else {
    rwd.classList.add('active');
    video.pause();
    intervalRwd = setInterval(windBackward, 200);
  }
}

function videoForward() {
  clearInterval(intervalRwd);
  rwd.classList.remove('active');

  if(fwd.classList.contains('active')) {
    fwd.classList.remove('active');
    clearInterval(intervalFwd);
    video.play();
  } else {
    fwd.classList.add('active');
    video.pause();
    intervalFwd = setInterval(windForward, 200);
  }
}

function windBackward() {
  if(video.currentTime <= 3) {
    rwd.classList.remove('active');
    clearInterval(intervalRwd);
    stopVideo();
  } else {
    video.currentTime -= 3;
  }
}

function windForward() {
  if(video.currentTime >= video.duration - 3) {
    fwd.classList.remove('active');
    clearInterval(intervalFwd);
    stopVideo();
  } else {
    video.currentTime += 3;
  }
}

function setTime() {
  let minutes = Math.floor(video.currentTime / 60);
  let seconds = Math.floor(video.currentTime - minutes * 60);
  let minuteValue;
  let secondValue;

  if (minutes < 10) {
    minuteValue = '0' + minutes;
  } else {
    minuteValue = minutes;
  }

  if (seconds < 10) {
    secondValue = '0' + seconds;
  } else {
    secondValue = seconds;
  }

 
  let videoTime = minuteValue + ':' + secondValue;
  timer.textContent = videoTime;

  let barLength = timerWrapper.clientWidth * (media.currentTime/media.duration);
  timerBar.style.width = barLength + 'px';
}


/****************** PROGRAMME  *****************/

// cibler la fenêtre
window.addEventListener('DOMContentLoaded', ()=>{
  console.log("DOM entièrement chargé et analysé"); 

  /*
  *supprimer les contrôles par défaut du navigateur sur la vidéo
  * rendre les nouveaux contrôles personnalisés
  */
  video.removeAttribute('controls');
  controls.style.visibility = 'visible';

  /*fonction play/pause au clic*/ 
  play.addEventListener('click', playPauseVideo);

  /*stopper la vidéo au clic du bouton stop*/
  stop.addEventListener('click', stopVideo);
  /*stopper la vidéo à la fin de la vidéo*/
  video.addEventListener('ended', stopVideo);

  /*barre de progression*/
  video.addEventListener('loadedmetadata', () => {
    progress.setAttribute('max', video.duration);
  });
  video.addEventListener('timeupdate', () => {
    if (!progress.getAttribute('max')) progress.setAttribute('max', video.duration);
    progress.value = video.currentTime;
    progressBar.style.width = `${Math.floor(video.currentTime * 100 / video.duration)}%`;
  });
    
  /*retour arrière et avance rapide*/
  rwd.addEventListener('click', videoBackward);
  fwd.addEventListener('click', videoForward);

  /*mise à jour du time code*/
  video.addEventListener('timeupdate', setTime);

  /*Couper le son*/
  mute.addEventListener('click', (e) => {
    video.muted = !video.muted;
  });

  /*Modifier le son*/
  volinc.addEventListener('click', (e) => {
    alterVolume('+');
  });
  voldec.addEventListener('click', (e) => {
    alterVolume('-');
  });
  
  rwd.classList.remove('active');
  fwd.classList.remove('active');
  clearInterval(intervalRwd);
  clearInterval(intervalFwd);
});
