<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230113105049 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE choregraphy ADD styles JSON NOT NULL, DROP style, CHANGE cours cours JSON NOT NULL');
        $this->addSql('ALTER TABLE music ADD styles JSON NOT NULL, DROP style');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE choregraphy ADD style VARCHAR(100) DEFAULT NULL, DROP styles, CHANGE cours cours VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE music ADD style VARCHAR(100) DEFAULT NULL, DROP styles');
    }
}
