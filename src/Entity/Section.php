<?php

namespace App\Entity;

use App\Repository\SectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SectionRepository::class)]
class Section
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\ManyToOne(inversedBy: 'section')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Choregraphy $choregraphy = null;

    #[ORM\OneToMany(mappedBy: 'section', targetEntity: Description::class)]
    private Collection $description;

    public function __construct()
    {
        $this->description = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getChoregraphy(): ?Choregraphy
    {
        return $this->choregraphy;
    }

    public function setChoregraphy(?Choregraphy $choregraphy): self
    {
        $this->choregraphy = $choregraphy;

        return $this;
    }

    /**
     * @return Collection<int, Description>
     */
    public function getDescription(): Collection
    {
        return $this->description;
    }

    public function addDescription(Description $description): self
    {
        if (!$this->description->contains($description)) {
            $this->description->add($description);
            $description->setSection($this);
        }

        return $this;
    }

    public function removeDescription(Description $description): self
    {
        if ($this->description->removeElement($description)) {
            // set the owning side to null (unless already changed)
            if ($description->getSection() === $this) {
                $description->setSection(null);
            }
        }

        return $this;
    }
}
