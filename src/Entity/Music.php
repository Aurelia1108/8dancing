<?php

namespace App\Entity;

use App\Repository\MusicRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MusicRepository::class)]
class Music
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    private ?string $artist = null;

    #[ORM\Column(nullable: true)]
    private ?int $intro = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\ManyToOne(inversedBy: 'music')]
    private ?Choregraphy $choregraphy = null;

    #[ORM\Column(nullable: true)]
    private ?array $styles = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getArtist(): ?string
    {
        return $this->artist;
    }

    public function setArtist(string $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    public function getIntro(): ?int
    {
        return $this->intro;
    }

    public function setIntro(?int $intro): self
    {
        $this->intro = $intro;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getChoregraphy(): ?Choregraphy
    {
        return $this->choregraphy;
    }

    public function setChoregraphy(?Choregraphy $choregraphy): self
    {
        $this->choregraphy = $choregraphy;

        return $this;
    }

    public function getStyles(): ?array
    {
        $styles = $this->styles;
        // $styles[] = '';

        return $styles;
    }

    public function setStyles (?array $styles): self
    {
        $this->styles = $styles;

        return $this;
    }
}
