<?php

namespace App\Entity;

use App\Entity\Music;
use App\Entity\Section;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ChoregraphyRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: ChoregraphyRepository::class)]
class Choregraphy
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $nameChoregraph = null;

    #[ORM\Column(nullable: true)]
    private ?int $nbCompte = null;

    #[ORM\Column(nullable: true)]
    private ?int $nbWall = null;

    #[ORM\Column(nullable: true)]
    private ?int $nbSection = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $remarque = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $modifiedAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $publicationAt = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $video = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $level = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\OneToMany(mappedBy: 'choregraphy', targetEntity: Music::class, cascade: ['persist'])]
    private Collection $music;

    #[ORM\OneToMany(mappedBy: 'choregraphy', targetEntity: Section::class, cascade: ['persist'])]
    private Collection $section;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $cours = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $styles = null;


    public function __construct()
    {
        $this->music = new ArrayCollection();
        $this->section = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getNameChoregraph(): ?string
    {
        return $this->nameChoregraph;
    }

    public function setNameChoregraph(?string $nameChoregraph): self
    {
        $this->nameChoregraph = $nameChoregraph;

        return $this;
    }

    public function getNbCompte(): ?int
    {
        return $this->nbCompte;
    }

    public function setNbCompte(?int $nbCompte): self
    {
        $this->nbCompte = $nbCompte;

        return $this;
    }

    public function getNbWall(): ?int
    {
        return $this->nbWall;
    }

    public function setNbWall(?int $nbWall): self
    {
        $this->nbWall = $nbWall;

        return $this;
    }

    public function getNbSection(): ?int
    {
        return $this->nbSection;
    }

    public function setNbSection(?int $nbSection): self
    {
        $this->nbSection = $nbSection;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeImmutable
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeImmutable $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getPublicationAt(): ?\DateTimeImmutable
    {
        return $this->publicationAt;
    }

    public function setPublicationAt(?\DateTimeImmutable $publicationAt): self
    {
        $this->publicationAt = $publicationAt;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(?string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(?string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, Music>
     */
    public function getMusic(): Collection
    {
        return $this->music;
    }

    public function addMusic(Music $music): self
    {
        if (!$this->music->contains($music)) {
            $this->music->add($music);
            $music->setChoregraphy($this);
        }

        return $this;
    }

    public function removeMusic(Music $music): self
    {
        if ($this->music->removeElement($music)) {
            // set the owning side to null (unless already changed)
            if ($music->getChoregraphy() === $this) {
                $music->setChoregraphy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Section>
     */
    public function getSection(): Collection
    {
        return $this->section;
    }

    public function addSection(Section $section): self
    {
        if (!$this->section->contains($section)) {
            $this->section->add($section);
            $section->setChoregraphy($this);
        }

        return $this;
    }

    public function removeSection(Section $section): self
    {
        if ($this->section->removeElement($section)) {
            // set the owning side to null (unless already changed)
            if ($section->getChoregraphy() === $this) {
                $section->setChoregraphy(null);
            }
        }

        return $this;
    }
    
    public function getStyles(): ?string
    {
        return $this->styles;
    }

    public function setStyles (?string $styles): self
    {
        $this->styles = $styles;

        return $this;
    }
    
    public function getCours(): ?string
    {
        return  $this->cours;
    }

    public function setCours(?string $cours): self
    {
        $this->cours = $cours;

        return $this;
    }
}
