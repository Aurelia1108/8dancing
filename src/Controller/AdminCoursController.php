<?php

namespace App\Controller;

use App\Entity\Cours;
use App\Form\CoursType;
use App\Repository\CoursRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/admin/cours')]
class AdminCoursController extends AbstractController
{
    #[Route('/', name: 'app_admin_cours_index', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function index(CoursRepository $coursRepository): Response
    {
        return $this->render('admin_cours/index.html.twig', [
            'cours' => $coursRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_cours_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_cours_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function new(Cours $cours=null, Request $request, CoursRepository $coursRepository, SluggerInterface $slugger): Response
    {
        if($cours == null)
            $cours = new Cours();
        $form = $this->createForm(CoursType::class, $cours);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cours->setSlug($slugger->slug($cours->getTitle())->lower());
            $coursRepository->save($cours, true);

            return $this->redirectToRoute('app_admin_cours_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_cours/new.html.twig', [
            'cours' => $cours,
            'form' => $form,
            'edit' => $cours->getId()

        ]);
    }

    #[Route('/{id}', name: 'app_admin_cours_show', methods: ['GET'])]
    #[IsGranted('ROLE_BUREAU')]
    public function show(Cours $cours): Response
    {
        return $this->render('admin_cours/show.html.twig', [
            'cours' => $cours,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_cours_delete', methods: ['POST'])]
    #[IsGranted('ROLE_PROF')]
    public function delete(Request $request, Cours $cours, CoursRepository $coursRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cours->getId(), $request->request->get('_token'))) {
            $coursRepository->remove($cours, true);
        }

        return $this->redirectToRoute('app_admin_cours_index', [], Response::HTTP_SEE_OTHER);
    }
}
