<?php

namespace App\Controller;

use App\Entity\Music;
use App\Form\MusicType;
use App\Repository\MusicRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/admin/music')]
class AdminMusicController extends AbstractController
{
    #[Route('/', name: 'app_admin_music_index', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function index(MusicRepository $musicRepository): Response
    {
        return $this->render('admin_music/index.html.twig', [
            'music' => $musicRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_music_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_music_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function new(Music $music=null, Request $request, MusicRepository $musicRepository, SluggerInterface $slugger): Response
    {
        if($music == null)
            $music = new Music();
        $form = $this->createForm(MusicType::class, $music);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $music->setSlug($slugger->slug($music->getTitle())->lower());
            $musicRepository->save($music, true);

            return $this->redirectToRoute('app_admin_music_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_music/new.html.twig', [
            'music' => $music,
            'form' => $form,
            'edit' => $music->getId()

        ]);
    }

    #[Route('/{id}', name: 'app_admin_music_show', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function show(Music $music): Response
    {
        return $this->render('admin_music/show.html.twig', [
            'music' => $music,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_music_delete', methods: ['POST'])]
    #[IsGranted('ROLE_PROF')]
    public function delete(Request $request, Music $music, MusicRepository $musicRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$music->getId(), $request->request->get('_token'))) {
            $musicRepository->remove($music, true);
        }

        return $this->redirectToRoute('app_admin_music_index', [], Response::HTTP_SEE_OTHER);
    }
}
