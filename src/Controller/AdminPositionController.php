<?php

namespace App\Controller;

use App\Entity\Position;
use App\Form\PositionType;
use App\Repository\PositionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/admin/position')]
class AdminPositionController extends AbstractController
{
    #[Route('/', name: 'app_admin_position_index', methods: ['GET'])]
    #[IsGranted('ROLE_BUREAU')]
    public function index(PositionRepository $positionRepository): Response
    {
        return $this->render('admin_position/index.html.twig', [
            'positions' => $positionRepository->findAll(),
        ]);
    }

    /* #[Route('/new', name: 'app_admin_position_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_position_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function new(Position $position=null,Request $request, PositionRepository $positionRepository): Response
    {
        if($position == null)
           $position = new Position();
            $form = $this->createForm(PositionType::class, $position);
            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $positionRepository->save($position, true);

            return $this->redirectToRoute('app_admin_position_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_position/new.html.twig', [
            'position' => $position,
            'form' => $form,
            'edit' => $position->getId()

        ]);
    } */

    #[Route('/{id}', name: 'app_admin_position_show', methods: ['GET'])]
    #[IsGranted('ROLE_BUREAU')]
    public function show(Position $position): Response
    {
        return $this->render('admin_position/show.html.twig', [
            'position' => $position,
        ]);
    }


    /* #[Route('/{id}', name: 'app_admin_position_delete', methods: ['POST'])]
    #[IsGranted('ROLE_PROF')]
    public function delete(Request $request, Position $position, PositionRepository $positionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$position->getId(), $request->request->get('_token'))) {
            $positionRepository->remove($position, true);
        }

        return $this->redirectToRoute('app_admin_position_index', [], Response::HTTP_SEE_OTHER);
    } */
    
}
