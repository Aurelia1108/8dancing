<?php

namespace App\Controller;


use App\Repository\UserRepository;
use App\Repository\CoursRepository;
use App\Repository\PositionRepository;
use App\Repository\ChoregraphyRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class BackController extends AbstractController
{
    #[Route('/admin/', name: 'espaceMembre')]
    #[IsGranted('ROLE_ELEVE')]
    public function prof(UserRepository $userRepository, PositionRepository $positionRepository, ChoregraphyRepository $choregraphyRepository): Response
    {
        return $this->render('Back/index.html.twig', [
            'controller_name' => 'backController', 
            'users' => $userRepository->findAll(),
            'positions' => $positionRepository->findAll(),
            'choregraphies' => $choregraphyRepository->findAll()


        ]);
    }




}
