<?php

namespace App\Controller;

use App\Entity\Info;
use App\Form\InfoType;
use DateTimeImmutable;
use App\Service\FileUploader;
use App\Repository\InfoRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/admin/info')]
class AdminInfoController extends AbstractController
{
    #[Route('/', name: 'app_admin_info_index', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function index(InfoRepository $infoRepository, UserRepository $userRepository): Response
    {
        //liste des infos privés
        return $this->render('admin_info/index.html.twig', [
            'infosPrivate' => $infoRepository->findBy(
                array('public'=> '0'),
                array('createdAt' => 'DESC'),
                10,
                0
            ),            
            'infosPublic' => $infoRepository->findBy(
                array('public'=> '1'),
                array('createdAt' => 'DESC'),
                10,
                0
            ),
            'users' => $userRepository->findAll(),
        ]);
                
    }

    #[Route('/new', name: 'app_admin_info_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_info_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_BUREAU')]
    public function new(Info $info=null, Request $request, InfoRepository $infoRepository,  SluggerInterface $slugger,  FileUploader $fileUploader): Response
    {
        if($info == null)
            $info = new Info();
        $form = $this->createForm(InfoType::class, $info);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($info->getId() == null) {
            /**création du slug à partir du titre de l'info */
                $info->setSlug($slugger->slug($info->getTitle())->lower());

            /** @var UploadedFile $infoFile */
                $infoFile = $form->get('picture')->getData();
                // dd($infoFile);

                /* cette condition est nécessaire car le champ 'image' n'est pas obligatoire
                * ainsi le fichier jgpeg, png ou gif doit être traité uniquement lorsqu'un fichier est téléchargé*/
                if ($infoFile) {
                    $infoFileName = $fileUploader->upload($infoFile, 'pictureInfo');
                // dd($pictureFileName);
                    // met à jour la propriété 'picture' pour stocker le nom du fichier PDF au lieu de son contenu
                    $info->setPicture($infoFileName);
                }
            /**Ajout automatique des dates de création ou de modification */
                $info->setCreatedAt(new \DateTimeImmutable('now'));

            }
            else{
                $info->setModifiedAt(new DateTimeImmutable('now'));
                /** @var UploadedFile $infoFile */
                $infoFile = $form->get('picture')->getData();
                // dd($infoFile);

                /* cette condition est nécessaire car le champ 'image' n'est pas obligatoire
                * ainsi le fichier jgpeg, png ou gif doit être traité uniquement lorsqu'un fichier est téléchargé*/
                if ($infoFile) {
                    $infoFileName = $fileUploader->upload($infoFile, 'pictureInfo');
                // dd($pictureFileName);
                    // met à jour la propriété 'picture' pour stocker le nom du fichier PDF au lieu de son contenu
                    $info->setPicture($infoFileName);
                }

            }

            $infoRepository->save($info, true);

            return $this->redirectToRoute('app_admin_info_index', [], Response::HTTP_SEE_OTHER);
            }

        return $this->renderForm('admin_info/new.html.twig', [
            'info' => $info,
            'form' => $form,
            'edit' => $info->getId()

        ]);
    }

    #[Route('/{id}', name: 'app_admin_info_show', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function show(Info $info): Response
    {
        return $this->render('admin_info/show.html.twig', [
            'info' => $info,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_info_delete', methods: ['POST'])] 
    #[IsGranted('ROLE_BUREAU')]
    public function delete(Request $request, Info $info, InfoRepository $infoRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$info->getId(), $request->request->get('_token'))) {
            $infoRepository->remove($info, true);
        }

        return $this->redirectToRoute('app_admin_info_index', [], Response::HTTP_SEE_OTHER);
    }
}
