<?php

namespace App\Controller;

use App\Repository\InfoRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(): Response
    {
        return $this->render('front/index.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/association', name: 'app_front_association')]
    public function association(): Response
    {
        return $this->render('front/pages/association.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/danses', name: 'app_front_danses')]
    public function danses(): Response
    {
        return $this->render('front/pages/danses.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/danses/rock', name: 'app_front_rock')]
    public function rock(): Response
    {
        return $this->render('front/pages/dansesRock.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/danses/chacha', name: 'app_front_chacha')]
    public function chacha(): Response
    {
        return $this->render('front/pages/dansesChacha.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/danses/salsa', name: 'app_front_salsa')]
    public function salsa(): Response
    {
        return $this->render('front/pages/dansesSalsa.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/danses/rumba', name: 'app_front_rumba')]
    public function rumba(): Response
    {
        return $this->render('front/pages/dansesRumba.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/danses/paso', name: 'app_front_paso')]
    public function paso(): Response
    {
        return $this->render('front/pages/dansesPaso.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/danses/groupe', name: 'app_front_groupe')]
    public function groupe(): Response
    {
        return $this->render('front/pages/dansesGroupe.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/evenements', name: 'app_front_evenements')]
    public function evenements(InfoRepository $infoRepository): Response
    {
        //liste des infos publiques
        return $this->render('front/pages/evenements.html.twig', [
            'controller_name' => 'FrontController',
            'infosPublic' => $infoRepository->findBy(
                array('public'=> '1'),
                array('createdAt' => 'DESC'),
                10,
                0
            )
        ]);
    }

    #[Route('/inscription', name: 'app_front_inscription')]
    public function inscription(): Response
    {
        return $this->render('front/pages/inscription.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/contact', name: 'app_front_contact')]
    public function contact(): Response
    {
        return $this->render('front/pages/contact.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }

    #[Route('/mentionsLegales', name: 'app_front_mentionsLegales')]
    public function mentionsLegales(): Response
    {
        return $this->render('front/pages/mentionsLegales.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }


}
