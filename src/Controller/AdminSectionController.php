<?php

namespace App\Controller;

use App\Entity\Section;
use App\Form\SectionType;
use App\Entity\Description;
use App\Repository\CompteRepository;
use App\Repository\SectionRepository;
use App\Repository\DescriptionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/section')]
class AdminSectionController extends AbstractController
{
    #[Route('/', name: 'app_admin_section_index', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function index(SectionRepository $sectionRepository, DescriptionRepository $descriptionRepository): Response
    {
        return $this->render('admin_section/index.html.twig', [
            'sections' => $sectionRepository->findAll(),
            'descriptions' => $descriptionRepository->findAll(),

        ]);
    }

    #[Route('/new', name: 'app_admin_section_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_section_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function new(Section $section=null, Request $request, SectionRepository $sectionRepository): Response
    {
        if($section == null)
            $section = new Section();
        $form = $this->createForm(SectionType::class, $section);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sectionRepository->save($section, true);

            return $this->redirectToRoute('app_admin_section_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_section/new.html.twig', [
            'section' => $section,
            'form' => $form,
            'edit' => $section->getId()

        ]);
    }

    #[Route('/{id}', name: 'app_admin_section_show', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function show(Section $section): Response
    {
        return $this->render('admin_section/show.html.twig', [
            'section' => $section,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_section_delete', methods: ['POST'])]
    #[IsGranted('ROLE_PROF')]
    public function delete(Request $request, Section $section, SectionRepository $sectionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$section->getId(), $request->request->get('_token'))) {
            $sectionRepository->remove($section, true);
        }

        return $this->redirectToRoute('app_admin_section_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/description', name: 'app_admin_section_description', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function listDescriptions(Section $section): Response
    {
        return $this->render('admin_section/listDescriptions.html.twig', [
            'section' => $section
        ]);
    }


    #[Route('/{id}/description/new', name: 'app_admin_section_description_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function newDescription(Section $section, Request $request, DescriptionRepository $descriptionRepository, CompteRepository $compteRepository): Response
    {
        $description = new Description();

        $form = $this->createForm(DescriptionType::class, $description);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $description->setSection($section);
            $descriptionRepository->save($description, true);

            return $this->redirectToRoute('app_admin_section_show', ['id'=> $section->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_description/new.html.twig', [
            'form' => $form,
            'section' => $section,
            'edit' => $description->getId(),
            'compte' => $compteRepository->findBy(
                array(),
                array('title' => 'ASC'),
                0,
                0
            )
        ]);
    }
}
