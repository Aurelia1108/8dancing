<?php

namespace App\Controller;

use App\Entity\Compte;
use App\Form\CompteType;
use App\Repository\CompteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/admin/compte')]
class AdminCompteController extends AbstractController
{
    #[Route('/', name: 'app_admin_compte_index', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function index(CompteRepository $compteRepository): Response
    {
        return $this->render('admin_compte/index.html.twig', [
            'comptes' => $compteRepository->findBy(
                array(),
                array('title' => 'ASC'),
                10,
                0
            )
        ]);
    }

    #[Route('/new', name: 'app_admin_compte_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_compte_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function new(Compte $compte=null, Request $request, CompteRepository $compteRepository): Response
    {
        if($compte == null)
            $compte = new Compte();
        $form = $this->createForm(CompteType::class, $compte);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $compteRepository->save($compte, true);

            return $this->redirectToRoute('app_admin_compte_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_compte/new.html.twig', [
            'compte' => $compte,
            'form' => $form,
            'edit' => $compte->getId()
        ]);
    }

    #[Route('/{id}', name: 'app_admin_compte_show', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function show(Compte $compte): Response
    {
        return $this->render('admin_compte/show.html.twig', [
            'compte' => $compte,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_compte_delete', methods: ['POST'])]
    #[IsGranted('ROLE_PROF')]
    public function delete(Request $request, Compte $compte, CompteRepository $compteRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$compte->getId(), $request->request->get('_token'))) {
            $compteRepository->remove($compte, true);
        }

        return $this->redirectToRoute('app_admin_compte_index', [], Response::HTTP_SEE_OTHER);
    }
}
