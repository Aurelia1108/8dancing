<?php

namespace App\Controller;

use App\Entity\Info;
use App\Entity\User;
use App\Form\InfoType;
use App\Form\UserType;
use DateTimeImmutable;
use App\Entity\Position;
use App\Form\PositionType;
use App\Repository\InfoRepository;
use App\Repository\UserRepository;
use App\Repository\PositionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\Regex;

#[Route('/admin/user')]
class AdminUserController extends AbstractController
{
    #[Route('/', name: 'app_admin_user_index', methods: ['GET'])]
    #[IsGranted('ROLE_BUREAU')]
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('admin_user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_user_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_user_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function new(User $user=null, Request $request, UserRepository $userRepository, UserPasswordHasherInterface $userPasswordHasher,  SluggerInterface $slugger): Response
    {
        if($user == null)
            $user = new User();

        $form = $this->createForm(UserType::class, $user);

        if ($user->getId() !== null)
            $form->remove('plainPassword');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($user->getId() == null) {
                $user->setPassword(
                    $userPasswordHasher->hashPassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );
                $user->setSlug($slugger->slug(($user->getLastname().' '.$user->getFirstname()))->lower());
                $user->setCreatedAt(new \DateTimeImmutable('now'));
            }
            else{
                $user->setModifiedAt(new DateTimeImmutable('now'));

            }

            $userRepository->save($user, true);

            return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_user/new.html.twig', [
            'user' => $user,
            'form' => $form,
            'edit' => $user->getId()

        ]);
    }

    #[Route('/{id}', name: 'app_admin_user_show', methods: ['GET'])]
    #[IsGranted('ROLE_BUREAU')]
    public function show(User $user): Response
    {
        return $this->render('admin_user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_user_delete', methods: ['POST'])]
    #[IsGranted('ROLE_PROF')]
    public function delete(Request $request, User $user, UserRepository $userRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $userRepository->remove($user, true);
        }

        return $this->redirectToRoute('app_admin_user_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/position', name: 'app_admin_user_position', methods: ['GET'])]
    #[IsGranted('ROLE_BUREAU')]
    public function listPositions(User $user): Response
    {
        return $this->render('admin_user/listPositions.html.twig', [
            'user' => $user,
            'id' => $user->getId()

        ]);
    }


    #[Route('/{id}/position/new', name: 'app_admin_user_position_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function newPosition(User $user, Request $request, PositionRepository $positionRepository): Response
    {        
        $position = new Position();

        $form = $this->createForm(PositionType::class, $position);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $position->setUser($user);
            $positionRepository->save($position, true);

            return $this->redirectToRoute('app_admin_user_position', ['id'=> $user->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_position/new.html.twig', [
            'form' => $form,
            'user' => $user,
            'edit' => $position->getId()
        ]);
    }

    #[Route('/{id}/position/{id_position}/edit', name: 'app_admin_position_edit', methods: ['GET', 'POST'])]
    #[Entity('position', expr: 'repository.find(id_position)')]
    #[IsGranted('ROLE_PROF')]
    public function editPosition(User $user, Position $position,  Request $request, PositionRepository $positionRepository): Response
    {        
        $form = $this->createForm(PositionType::class, $position);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $position->setUser($user);
            $positionRepository->save($position, true);

            return $this->redirectToRoute('app_admin_user_position', ['id'=> $user->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_position/new.html.twig', [
            'form' => $form,
            'user' => $user,
            'position' => $position,
            'edit' => true
        ]);
    }

    #[Route('/{id}/position/{id_position}/delete', name: 'app_admin_position_delete', methods: ['POST'])]
    #[Entity('position', expr: 'repository.find(id_position)')]
    #[IsGranted('ROLE_PROF')]
    public function deletePosition(User $user, Request $request, Position $position, PositionRepository $positionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$position->getId(), $request->request->get('_token'))) {
            $positionRepository->remove($position, true);
        }

        return $this->redirectToRoute('app_admin_user_position', ['id'=>$user->getId()], Response::HTTP_SEE_OTHER);
    }


    #[Route('/{id}/info', name: 'app_admin_user_info', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function listInfos(User $user): Response
    {
        return $this->render('admin_user/listInfos.html.twig', [
            'user' => $user
        ]);
    }


    #[Route('/{id}/info/new', name: 'app_admin_user_info_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_BUREAU')]
    public function newInfo(User $user, Request $request, InfoRepository $infoRepository): Response
    {
        $info = new Info();

        $form = $this->createForm(InfoType::class, $info);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $info->setUser($user);
            $infoRepository->save($info, true);

            return $this->redirectToRoute('app_admin_user_show', ['id'=> $user->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_info/new.html.twig', [
            'form' => $form,
            'user' => $user,
            'edit' => $info->getId()
        ]);
    }


}
