<?php

namespace App\Controller;

use App\Entity\Description;
use App\Form\DescriptionType;
use App\Repository\DescriptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/admin/description')]
class AdminDescriptionController extends AbstractController
{
    #[Route('/', name: 'app_admin_description_index', methods: ['GET'])]
    #[IsGranted('ROLE_PROF')]
    public function index(DescriptionRepository $descriptionRepository): Response
    {
        return $this->render('admin_description/index.html.twig', [
            'descriptions' => $descriptionRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_description_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_description_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function new(Description $description=null, Request $request, DescriptionRepository $descriptionRepository): Response
    {
        if($description == null)
            $description = new Description();
        $form = $this->createForm(DescriptionType::class, $description);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $descriptionRepository->save($description, true);

            return $this->redirectToRoute('app_admin_description_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_description/new.html.twig', [
            'description' => $description,
            'form' => $form,
            'edit' => $description->getId()

        ]);
    }

    #[Route('/{id}', name: 'app_admin_description_show', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function show(Description $description): Response
    {
        return $this->render('admin_description/show.html.twig', [
            'description' => $description,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_description_delete', methods: ['POST'])]
    #[IsGranted('ROLE_PROF')]
    public function delete(Request $request, Description $description, DescriptionRepository $descriptionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$description->getId(), $request->request->get('_token'))) {
            $descriptionRepository->remove($description, true);
        }

        return $this->redirectToRoute('app_admin_description_index', [], Response::HTTP_SEE_OTHER);
    }
}
