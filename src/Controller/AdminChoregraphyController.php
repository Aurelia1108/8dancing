<?php

namespace App\Controller;

use App\Entity\Music;
use DateTimeImmutable;
use App\Entity\Section;
use App\Form\MusicType;
use App\Form\SectionType;
use App\Entity\Choregraphy;
use App\Form\ChoregraphyType;
use Doctrine\ORM\Mapping\Entity;
use App\Repository\MusicRepository;
use App\Repository\SectionRepository;
use App\Repository\ChoregraphyRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/choregraphy')]
class AdminChoregraphyController extends AbstractController
{
    #[Route('/', name: 'app_admin_choregraphy_index', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function index(ChoregraphyRepository $choregraphyRepository): Response
    {
        return $this->render('admin_choregraphy/index.html.twig', [
            'choregraphies' => $choregraphyRepository->findBy(
                array(),
                array('title' => 'ASC'),
                10,
                0
            )
        ]);
    }

    #[Route('/new', name: 'app_admin_choregraphy_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_choregraphy_edit', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function new(Choregraphy $choregraphy=null, Request $request, ChoregraphyRepository $choregraphyRepository,  SluggerInterface $slugger): Response
    {
        if($choregraphy == null)
            $choregraphy = new Choregraphy();
            
        $form = $this->createForm(ChoregraphyType::class, $choregraphy);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($choregraphy->getId() == null) {
                $choregraphy->setNbSection(count($choregraphy->getSection()));
                $choregraphy->setSlug($slugger->slug($choregraphy->getTitle())->lower());
                $choregraphy->setCreatedAt(new \DateTimeImmutable('now'));
            }
            else{
                $choregraphy->setModifiedAt(new DateTimeImmutable('now'));

            }

            //dd($choregraphy->getMusic());

            $choregraphyRepository->save($choregraphy, true);

            return $this->redirectToRoute('app_admin_choregraphy_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_choregraphy/new.html.twig', [
            'choregraphy' => $choregraphy,
            'form' => $form,
            'edit' => $choregraphy->getId()

        ]);
    }

    #[Route('/{id}', name: 'app_admin_choregraphy_show', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]

    public function show(Choregraphy $choregraphy): Response
    {
        return $this->render('admin_choregraphy/show.html.twig', [
            'choregraphy' => $choregraphy,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_choregraphy_delete', methods: ['POST'])]
    #[IsGranted('ROLE_PROF')]
    public function delete(Request $request, Choregraphy $choregraphy, ChoregraphyRepository $choregraphyRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$choregraphy->getId(), $request->request->get('_token'))) {
            $choregraphyRepository->remove($choregraphy, true);
        }

        return $this->redirectToRoute('app_admin_choregraphy_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/section', name: 'app_admin_choregraphy_section', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function listSections(Choregraphy $choregraphy): Response
    {
        return $this->render('admin_choregraphy/listSections.html.twig', [
            'choregraphy' => $choregraphy,
            'id' => $choregraphy->getId()
        ]);

    }

    #[Route('/{id}/section/new', name: 'app_admin_choregraphy_section_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function newSection(Choregraphy $choregraphy, Request $request, SectionRepository $sectionRepository): Response
    {
        $section = new Section();

        $form = $this->createForm(SectionType::class, $section);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $section->setChoregraphy($choregraphy);
            $sectionRepository->save($section, true);

            return $this->redirectToRoute('app_admin_choregraphy_show', ['id'=> $choregraphy->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_section/new.html.twig', [
            'form' => $form,
            'choregraphy' => $choregraphy,
            'edit' => $section->getId(),
        ]);
    }

    #[Route('/{id}/section/{id_section}/edit', name: 'app_admin_choregraphy_section_edit', methods: ['GET', 'POST'])]
    #[Entity('section', expr: 'repository.find(id_section)')]
    #[IsGranted('ROLE_PROF')]
    public function editSection(Choregraphy $choregraphy, Section $section, Request $request, SectionRepository $sectionRepository): Response
    {
        $form = $this->createForm(SectionType::class, $section);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $section->setChoregraphy($choregraphy);
            $sectionRepository->save($section, true);

            return $this->redirectToRoute('app_admin_choregraphy_section', ['id'=> $choregraphy->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_section/new.html.twig', [
            'form' => $form,
            'choregraphy' => $choregraphy,
            'section' =>$section,
            'edit' => true,
        ]);
    }
    #[Route('/{id}/section/{id_section}/delete', name: 'app_admin_section_delete', methods: ['POST'])]
    #[Entity('section', expr: 'repository.find(id_section)')]
    #[IsGranted('ROLE_PROF')]
    public function deleteSection(Choregraphy $choregraphy, Request $request, Section $section, SectionRepository $sectionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$section->getId(), $request->request->get('_token'))) {
            $sectionRepository->remove($section, true);
        }

        return $this->redirectToRoute('app_admin_user_section', ['id'=>$choregraphy->getId()], Response::HTTP_SEE_OTHER);
    }


    #[Route('/{id}/music', name: 'app_admin_choregraphy_music', methods: ['GET'])]
    #[IsGranted('ROLE_ELEVE')]
    public function listMusics(Choregraphy $choregraphy, MusicRepository $musicRepository): Response
    {
        return $this->render('admin_choregraphy/listMusics.html.twig', [
            'choregraphy' => $choregraphy,
            'musics' => $musicRepository->findBy(
                array(),
                array('title' => 'ASC'),
                10,
                0
            )
        ]);
    }


    #[Route('/{id}/music/new', name: 'app_admin_choregraphy_music_new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_PROF')]
    public function newMusic(Choregraphy $choregraphy, Request $request, MusicRepository $musicRepository, SluggerInterface $slugger): Response
    {
        $music = new Music();

        $form = $this->createForm(MusicType::class, $music);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $music->setSlug($slugger->slug(($music->getTitle()))->lower());
            $music->setChoregraphy($choregraphy);
            $musicRepository->save($music, true);

            return $this->redirectToRoute('app_admin_choregraphy_show', ['id'=> $choregraphy->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_music/new.html.twig', [
            'form' => $form,
            'choregraphy' => $choregraphy,
            'edit' => $music->getId()
        ]);
    }
    
    #[Route('/{id}/music/{id_music}/edit', name: 'app_admin_choregraphy_music_edit', methods: ['GET', 'POST'])]
    #[Entity('music', expr: 'repository.find(id_music)')]
    #[IsGranted('ROLE_PROF')]
    public function editMusic(Choregraphy $choregraphy, Music $music, Request $request, MusicRepository $musicRepository): Response
    {
        $form = $this->createForm(MusicType::class, $music);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $music->setChoregraphy($choregraphy);
            $musicRepository->save($music, true);

            return $this->redirectToRoute('app_admin_choregraphy_music', ['id'=> $choregraphy->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_music/new.html.twig', [
            'form' => $form,
            'choregraphy' => $choregraphy,
            'music' =>$music,
            'edit' => true,
        ]);
    }
    #[Route('/{id}/music/{id_music}/delete', name: 'app_admin_music_delete', methods: ['POST'])]
    #[Entity('music', expr: 'repository.find(id_music)')]
    #[IsGranted('ROLE_PROF')]
    public function deletemusic(Choregraphy $choregraphy, Request $request, Music $music, MusicRepository $musicRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$music->getId(), $request->request->get('_token'))) {
            $musicRepository->remove($music, true);
        }

        return $this->redirectToRoute('app_admin_user_music', ['id'=>$choregraphy->getId()], Response::HTTP_SEE_OTHER);
    }
}
