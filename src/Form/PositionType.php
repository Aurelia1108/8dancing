<?php

namespace App\Form;

use App\Entity\Cours;
use App\Entity\Position;
use App\Repository\CoursRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PositionType extends AbstractType
{
    private $coursRepository;

    public function __construct(CoursRepository $coursRepository)
    {
        $this -> coursRepository = $coursRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('position', ChoiceType::class, array(
                'required' => true,
                'choices' => array('Duo meneur' => 'POSITION_MENEUR', 'Duo Suiveur' => 'POSITION_SUIVEUR', 'Groupe' => 'POSITION_GROUPE')
            ))
            ->add('cours', EntityType::class,[
                'class' => Cours::class,
            // liste des cours en fonction de la table cours
                'choices' => $this->coursRepository->findAll(),
                'required' => true,
            // uses the Cours.title property as the visible option string
                'choice_label' => 'title',
                'multiple' => false
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Position::class,
        ]);
    }
}
