<?php

namespace App\Form;

use App\Entity\Music;
use App\Entity\Section;
use App\Entity\Choregraphy;
use App\Repository\SectionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ChoregraphyType extends AbstractType
{
    private $sectionRepository;

    public function __construct(SectionRepository $sectionRepository)
    {
        $this -> sectionRepository = $sectionRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('nameChoregraph')
            ->add('nbCompte')
            ->add('nbWall')
            ->add('remarque')
            ->add('video', FileType::class,[
                'label' => 'Vidéo',
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '500M',
                        'mimeTypes' => [
                                'video/mpeg', 
                                'video/mp4',
                                'video/quicktime',
                                'video/x-ms-wmv', 'video/x-msvideo', 'video/x-flv'
                        ],
                        'mimeTypesMessage' => 'ce format de video est inconnu',
                        'maxSizeMessage' => "fichier trop volumineux"
                    ])
                ]
            ])
            ->add('level', ChoiceType::class, array(
                'required' => false,
                'multiple' => false,
                'choices' => array('Débutant' => 'Débutant', 'Intermédiare' => 'Intermédiare', 'Confirmé' => 'Confirmé')
            ))
            ->add('cours', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array('Duo' => 'DUO', 'Groupe' => 'GROUPE')
            ))
            ->add('styles', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array('Autre'=>'AUTRE', 'Chacha' => 'CHACHA', 'Paso Doble' => 'PASO DOBLE', 'Quick Step' => 'QUICKSTEP', 'Rock' => 'ROCK', 'Rumba' => 'RUMBA', 'Salsa' => 'SALSA', 'Samba' => 'SAMBA', 'Slow-Fox' => 'SLOWFOX', 'Tango'=> 'TANGO', 'Valse' => 'VALSE')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Choregraphy::class,
        ]);
    }
}
