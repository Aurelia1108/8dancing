<?php

namespace App\Form;

use App\Entity\Section;
use App\Entity\Description;
use Symfony\Component\Form\AbstractType;
use App\Repository\DescriptionRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SectionType extends AbstractType
{

    private $descriptionRepository;

    public function __construct(DescriptionRepository $descriptionRepository)
    {
        $this -> descriptionRepository = $descriptionRepository;

    }

    
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('description', EntityType::class,[
                'class' => Description::class,
            // liste des descrition en fonction de la table Description
                'choices' => $this->descriptionRepository->findAll(),
                'required' => false,
            // uses the Description.descrition property as the visible option string
                'choice_label' => function($description){
                    return $description->getCompte()->getTitle(). ' > ' . $description->getDescription(); 
                },
                'multiple' => true
                ])    
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Section::class,
        ]);
    }
}
