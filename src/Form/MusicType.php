<?php

namespace App\Form;

use App\Entity\Music;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MusicType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('artist')
            ->add('intro')
            ->add('styles', ChoiceType::class, array(
                'required' => false,
                'multiple' => true,
                'choices' => array('Autre' => 'AUTRE', 'Chacha' => 'CHACHA', 'Paso Doble' => 'PASO DOBLE', 'Quick Step' => 'QUICKSTEP', 'Rock' => 'ROCK', 'Rumba' => 'RUMBA', 'Salsa' => 'SALSA', 'Samba' => 'SAMBA', 'Slow-Fox' => 'SLOWFOX', 'Tango'=> 'TANGO', 'Valse' => 'VALSE')
            ))
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Music::class,
        ]);
    }
}
