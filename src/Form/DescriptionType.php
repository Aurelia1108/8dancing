<?php

namespace App\Form;

use App\Entity\Compte;
use App\Entity\Section;
use App\Entity\Description;
use App\Repository\CompteRepository;
use App\Repository\SectionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DescriptionType extends AbstractType
{
    private $compteRepository;

    public function __construct(CompteRepository $compteRepository)
    {
        $this -> compteRepository = $compteRepository;
    }

    private $sectionRepository;

    public function __constructSection(SectionRepository $sectionRepository)
    {
        $this -> sectionRepository = $sectionRepository;
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('compte', EntityType::class,[
                'class' => Compte::class,
            // liste des comptes en fonction de la table Compte
                'required' => false,
            // uses the Compte.title property as the visible option string
                'choice_label' => 'title',
                'multiple' => false,
                // 'order' => 'ASC'
            ])
            ->add('section', EntityType::class,[
                'class' => Section::class,
            // liste des sections en fonction de la table Section
                'required' => false,
            // uses the Section.title property as the visible option string
                'choice_label' => 'title',
                'multiple' => false,
                // 'order' => 'ASC'
            ])
            ->add('description')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Description::class,
        ]);
    }
}
