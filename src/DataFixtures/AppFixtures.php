<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    protected $slugger;

    protected $userPasswordHasher;
        
    public function __construct(SluggerInterface $slugger, UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->slugger = $slugger;
        $this->userPasswordHasher = $userPasswordHasher;
    }

    public function load(ObjectManager $manager): void
    {

        $faker = Factory::create('fr_FR');  
        for($i=0;$i<5;$i++){
            $user = new User();
            $user->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setBirthdayAt(new DateTimeImmutable())
            ->setAdress1($faker->address())
            ->setZipCode($faker->randomNumber(5, false))
            ->setCity($faker->city())
            ->setPhone($faker->mobileNumber())
            ->setEmail($faker->email())
            ->setPassword($this->userPasswordHasher->hashPassword($user, '123456'))
            ->setCreatedAt(new DateTimeImmutable())
            ->setSlug($this->slugger->slug(($user->getLastname().' '.$user->getFirstname()))->lower())

            ->setRoles(["ROLE_BUREAU"]); 
            $manager->persist($user);
        }
        for($j=0;$j<10;$j++){
            $user = new User();
            $user->setFirstname($faker->firstName)
            ->setLastname($faker->lastName)
            ->setBirthdayAt(new DateTimeImmutable())
            ->setAdress1($faker->address())
            ->setZipCode($faker->randomNumber(5, false))
            ->setCity($faker->city())
            ->setPhone($faker->mobileNumber())
            ->setEmail($faker->email())
            ->setPassword($this->userPasswordHasher->hashPassword($user, '123456'))
            ->setCreatedAt(new DateTimeImmutable())
            ->setSlug($this->slugger->slug(($user->getLastname().' '.$user->getFirstname()))->lower())
            ->setRoles(["ROLE_ELEVE"]);           
            
            $manager->persist($user);
        }
              
        $manager->flush();
    }
}